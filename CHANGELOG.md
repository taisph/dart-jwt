## 0.1.2

* make typ header optional and default to JWT

## 0.1.1

* Add audience claim

## 0.1.0+2

* Bug fix. Had dependency on sdk 1.3 without realising it. Changed sdk version in
pubspec.yaml

